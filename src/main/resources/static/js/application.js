'use strict'
var app = angular.module('myApp',  ['ngResource', 'ngRoute', 'http-auth-interceptor']);

app.constant('USER_ROLES', {
	all: '*',
	admin: 'admin',
	user: 'user'
});


app.config(function ($routeProvider, USER_ROLES) {

	$routeProvider.when('/main', {
		templateUrl: 'partials/main.html',
		controller: 'MainController',
		access: {
			loginRequired: false,
			authorizedRoles: [USER_ROLES.all]
		}
	}).when('/', {
		redirectTo: '/main'
	}).when('/login', {
		templateUrl: 'partials/login.html',
		controller: 'LoginController',
		access: {
			loginRequired: false,
			authorizedRoles: [USER_ROLES.all]
		}
	}).when("/loading", {
		templateUrl: 'partials/loading.html',
		access: {
			loginRequired: false,
			authorizedRoles: [USER_ROLES.all]
		}
	}).when("/logout", {
		template: " ",
		controller: 'LogoutController',
		access: {
			loginRequired: false,
			authorizedRoles: [USER_ROLES.all]
		}
	}).when("/error/:code", {
		templateUrl: 'partials/error.html',
		controller: 'ErrorController',
		access: {
			loginRequired: false,
			authorizedRoles: [USER_ROLES.all]
		}
	}).when("/register", {
		templateUrl: 'partials/register.html',
		controller: 'RegisterController',
		access: {
			loginRequired: false,
			authorizedRoles: [USER_ROLES.all]
		}
	}).when("/profile", {
		templateUrl: 'partials/user_overview.html',
		controller: 'OverviewController',
		access: {
			loginRequired: true,
			authorizedRoles: [USER_ROLES.all]
		}
	}).when("/notes", {
		templateUrl: 'partials/user_notes.html',
		controller: 'UserNotesController',
		access: {
			loginRequired: true,
			authorizedRoles: [USER_ROLES.all]
		}
	}).when("/note/create", {
		templateUrl: 'partials/create_note.html',
		controller: 'AddNoteController',
		access: {
			loginRequired: true,
			authorizedRoles: [USER_ROLES.all]
		}
	}).when("/note/:noteId/edit", {
		templateUrl: 'partials/edit_note.html',
		controller: 'EditNoteController',
		access: {
			loginRequired: true,
			authorizedRoles: [USER_ROLES.all]
		}
	}).when("/note/:noteId/chapter/create", {
        templateUrl: 'partials/create_chapter.html',
        controller: 'CreateChapterController',
        access: {
            loginRequired: true,
            authorizedRoles: [USER_ROLES.all]
        }
    }).when("/chapter/:chapterId/edit", {
        templateUrl: 'partials/edit_chapter.html',
        controller: 'EditChapterController',
        access: {
            loginRequired: true,
            authorizedRoles: [USER_ROLES.all]
        }
    })
		.otherwise({
			redirectTo: 'error/:404',
			access: {
				loginRequired: false,
				authorizedRoles: [USER_ROLES.all]
			}
		})
	});

app.run(function ($rootScope, $location, $http, AuthSharedService, Session, USER_ROLES, $q, $timeout) {

	$rootScope.$on('$routeChangeStart', function (event, next) {

		if (next.originalPath === "/login" && $rootScope.authenticated) {
			event.preventDefault();
		} else if (next.access && next.access.loginRequired && !$rootScope.authenticated) {
			event.preventDefault();
			$rootScope.$broadcast("event:auth-loginRequired", {});
		} else if (next.access && !AuthSharedService.isAuthorized(next.access.authorizedRoles)) {
			event.preventDefault();
			$rootScope.$broadcast("event:auth-forbidden", {});
		}
	});

	$rootScope.$on('$routeChangeSuccess', function ($scope, next, current) {
		$rootScope.$evalAsync(function () {
			$.material.init();
		});
	});

	// Call when the the client is confirmed
	$rootScope.$on('event:auth-loginConfirmed', function (event, data) {
		console.log('login confirmed start ' + data);
		$rootScope.loadingAccount = false;
		var nextLocation = ($rootScope.requestedUrl ? $rootScope.requestedUrl : "/main");
		var delay = ($location.path() === "/loading" ? 1500 : 0);

		$timeout(function () {
			Session.create(data);
			$rootScope.account = Session;
			$rootScope.authenticated = true;
			$location.path(nextLocation).replace();
		}, delay);

	});

	// Call when the 401 response is returned by the server
	$rootScope.$on('event:auth-loginRequired', function (event, data) {
		if ($rootScope.loadingAccount && data.status !== 401) {
			$rootScope.requestedUrl = $location.path()
			$location.path('/loading');
		} else {
			Session.invalidate();
			$rootScope.authenticated = false;
			$rootScope.loadingAccount = false;
			$location.path('/login');
		}
	});

	// Call when the 403 response is returned by the server
	$rootScope.$on('event:auth-forbidden', function (rejection) {
		$rootScope.$evalAsync(function () {
			$location.path('/error/403').replace();
		});
	});

	// Call when the user logs out
	$rootScope.$on('event:auth-loginCancelled', function () {
		$location.path('/login').replace();
	});

	// Get already authenticated user account
	AuthSharedService.getAccount();
	
	$rootScope.changeToLight = function () {
		document.getElementById('bootstrap_theme').href = 'css/flatly.bootstrap.min.css';
		document.getElementById('login_theme').href = "css/login_light.css";
		document.getElementById('profile_theme').href = "css/user_profile_light.css";
		localStorage.setItem('theme', 'light');
	}

	$rootScope.changeToDark = function () {
		document.getElementById('bootstrap_theme').href = 'css/darkly.bootstrap.min.css';
		document.getElementById('login_theme').href = "css/login_dark.css";
		document.getElementById('profile_theme').href = "css/user_profile_dark.css";
		localStorage.setItem('theme', 'dark');
	}

	if(localStorage.getItem('theme') == 'dark'){
		$rootScope.changeToDark();
	}

	if(localStorage.getItem('theme') == 'light'){
		$rootScope.changeToLight();
	}

});
