

app.service('UserService', function($http){
	return {
		register: function (user) {
			$http.post('register', user)
				.success(function (data, status, headers, config) {
					$http.get("partials/login.html")
				})
		}
	}

});

app.service('Session', function () {
	this.create = function (data) {
		this.id = data.id;
		this.login = data.username;
		this.userRoles = [];
		angular.forEach(data.authorities, function (value, key) {
			this.push(value.name);
		}, this.userRoles);
	};
	this.invalidate = function () {
		this.id = null;
		this.login = null;
		this.userRoles = null;
	};
	return this;
});

app.service('AuthSharedService', function ($rootScope, $http, $resource, authService, Session) {
	return {
		login: function (userName, password) {
			var config = {
				ignoreAuthModule: 'ignoreAuthModule',
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			};
			$http.post('authenticate', $.param({
				username: userName,
				password: password,
			}), config)
				.success(function (data, status, headers, config) {
					authService.loginConfirmed(data);
				})
				.error(function (data, status, headers, config) {
					$rootScope.authenticationError = true;
					Session.invalidate();
				});
		},
		getAccount: function () {
			$rootScope.loadingAccount = true;
			$http.get('security/account')
				.then(function (response) {
					authService.loginConfirmed(response.data);
				});
		},
		isAuthorized: function (authorizedRoles) {
			if (!angular.isArray(authorizedRoles)) {
				if (authorizedRoles == '*') {
					return true;
				}
				authorizedRoles = [authorizedRoles];
			}
			var isAuthorized = false;
			angular.forEach(authorizedRoles, function (authorizedRole) {
				var authorized = (!!Session.login &&
				Session.userRoles.indexOf(authorizedRole) !== -1);
				if (authorized || authorizedRole == '*') {
					isAuthorized = true;
				}
			});
			return isAuthorized;
		},
		logout: function () {
			$rootScope.authenticationError = false;
			$rootScope.authenticated = false;
			$rootScope.account = null;
			$http.get('logout');
			Session.invalidate();
			authService.loginCancelled();
		}
	};
});

app.service('NoteService', function ($http,  $window) {
    return {
        createChapter: function (chapter, userId, noteId) {
            $http.post('chapter', $.param({
                chapter: chapter,
                userId: userId,
                noteId: noteId
            }), config)
                .success(function (data, status, headers, config) {
                })
        },
        createNote: function (note, userId) {
            $http.post('/' + userId + '/note', note).success(function (data, status) {
				$window.location.href = "#/notes";
			});
        },
		editNote: function(note, userId){
			$http.put('/' + userId + '/note/' + note.id, note).success(function (data, status) {
				$window.location.href = "#/notes";
			})
		},
		
		getNote: function (noteId, userId) {
			$http.get('/' + userId + '/note/' + noteId).success(function (data, status) {
				return data;
			})
		},

		getUserNotes: function (userId) {
			$http.get('/' + userId + '/notes/').success(function (data, status) {
				return data;
			})
		}
    };
});

app.service('ChapterService', function ($http, $window) {
    return{
        createChapter: function (noteId, chapter) {
            $http.post('/note/' + noteId + '/chapter/', chapter).success(function () {
                $window.location.href = "#/note/" + noteId + "/edit";
            });
        }
    }
});