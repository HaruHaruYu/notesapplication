app.controller('MainController', function($http,$scope){
	$http.get('/notes').success(function (data) {
		$scope.notes = data;
	});

	$scope.showChapter = function(chapter){
		if (chapter.show == undefined)
			chapter.show = true;
		else chapter.show = !chapter.show;
	}
});

app.controller('OverviewController', function ($rootScope, $scope) {
	var session = $rootScope.account;
	$scope.username = session.login;
});

app.controller('UserNotesController', function ($rootScope, $scope, NoteService, $http, $window) {
	var session = $rootScope.account;
	$scope.username = session.login;
	$scope.userId = session.id;
	$http.get('/' + $scope.userId + '/notes/').success(function (data, status) {
		$scope.notes = data;
	});
	$scope.deleteNote = function(note){
		$http.delete('/' + $scope.userId + '/note/' + note.id).success(function (data) {
			$scope.notes = data;
		})
	}

	$scope.editNote = function(note){
        $window.location.href = "#/note/" + note.id + "/edit";
    }
});

app.controller('AddNoteController', function ($rootScope, $scope, NoteService) {
	var session = $rootScope.account;
	$scope.username = session.login;
	$scope.userId = session.id;
	$scope.createNote = function () {
		$scope.note.date = new Date();
		NoteService.createNote($scope.note, $scope.userId);
	}
});

app.controller('EditNoteController', function ($rootScope, $scope, $http, $routeParams, $window, NoteService) {
    var session = $rootScope.account;
    $scope.username = session.login;
    $scope.userId = session.id;
    var id = $routeParams.noteId;
    $http.get("/" + $scope.userId + "/note/" + id).success(function (data) {
        $scope.note = data;
    });
    $scope.editNote = function () {
        $scope.note.date = new Date();
        NoteService.editNote($scope.note, $scope.userId);
    }

    $scope.addChapter = function () {
        $window.location.href = '#/note/' + id + "/chapter/create"
    }
});

app.controller('CreateChapterController', function ($rootScope, $scope, $http,$routeParams, ChapterService){
    var noteId = $routeParams.noteId;
    $scope.createChapter = function(){
        ChapterService.createChapter(noteId, $scope.chapter);
    }
});

app.controller('EditChapterController', function ($rootScope, $scope, $http,$routeParams, ChapterService) {

});

app.controller('RegisterController', function($scope, UserService){
	$scope.register = function(){
		UserService.register($scope.user);
	}
});

app.controller('LoginController', function($rootScope, $scope, AuthSharedService) {
	$scope.login = function() {
		$rootScope.authenticationError = false;
		AuthSharedService.login($scope.username, $scope.password);
	}
});

app.controller('LogoutController', function (AuthSharedService) {
	AuthSharedService.logout();
});

app.controller('ErrorController', function ($scope, $routeParams) {
	$scope.code = $routeParams.code;

	switch ($scope.code) {
		case "403" :
			$scope.message = "Oops! you have come to unauthorised page."
			break;
		case "404" :
			$scope.message = "Page not found."
			break;
		default:
			$scope.code = 500;
			$scope.message = "Oops! unexpected error"
	}
});