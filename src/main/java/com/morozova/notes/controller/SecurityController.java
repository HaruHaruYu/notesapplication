package com.morozova.notes.controller;

import com.morozova.notes.model.User;
import com.morozova.notes.repository.UserRepository;
import com.morozova.notes.service.UserService;
import com.morozova.notes.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by Flizaita on 19.11.2016.
 */
@Controller
public class SecurityController{

        @Autowired
        UserService userService;

        @RequestMapping(value = "/security/account", method = RequestMethod.GET)
        public @ResponseBody
        User getUserAccount()  {
            User user = userService.findByEmail(SecurityUtils.getCurrentLogin());
            user.setPassword(null);
            return user;
        }
}
