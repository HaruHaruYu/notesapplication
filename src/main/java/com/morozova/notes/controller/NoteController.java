package com.morozova.notes.controller;

import com.morozova.notes.model.Chapter;
import com.morozova.notes.model.Note;
import com.morozova.notes.model.User;
import com.morozova.notes.repository.ChapterRepository;
import com.morozova.notes.repository.NoteRepository;
import com.morozova.notes.service.UserService;
import com.morozova.notes.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collection;
import java.util.List;

/**
 * Created by Flizaita on 22.11.2016.
 */
@RestController
public class NoteController {

    @Autowired
    NoteRepository noteRepository;

    @Autowired
    UserService userService;

    @Autowired
    ChapterRepository chapterRepository;

    @RequestMapping(value = "/notes", method = RequestMethod.GET)
    Iterable<Note> getAllNotes(){
        return noteRepository.findAll();
    }

    @RequestMapping(value = "/{userId}/note", method = RequestMethod.POST)
    @ResponseBody
    void createNote(@PathVariable Long userId, @RequestBody Note note, HttpServletResponse response) throws IOException{
        User user = userService.getUserById(userId);
        note.setUser(user);
        Note el = noteRepository.save(note);
        SecurityUtils.sendResponse(response, HttpServletResponse.SC_OK, el.getId());
    }

    @RequestMapping(value = "/{userId}/notes", method = RequestMethod.GET)
    Collection<Note> getNotes(@PathVariable Long userId) throws IOException{

        Collection<Note> notes = noteRepository.findByUserId(userId);
        return notes;
    }

    @RequestMapping(value = "/{userId}/note/{noteId}", method = RequestMethod.DELETE)
    Collection<Note> deleteNote(@PathVariable Long userId, @PathVariable Long noteId){
        Note note = noteRepository.findOne(noteId);
        System.out.print(note.getUser());
        noteRepository.delete(noteId);
        Collection<Note> notes = noteRepository.findByUserId(userId);
        return notes;
    }

    @RequestMapping(value = "/{userId}/note/{noteId}", method = RequestMethod.GET)
    Note getNote(@PathVariable Long userId, @PathVariable Long noteId) {
        Note note = noteRepository.findById(noteId);
        return note;

    }

    @RequestMapping(value = "/{userId}/note/{noteId}", method = RequestMethod.PUT)
    void editNote(@PathVariable Long userId, @PathVariable Long noteId, @RequestBody Note note){
        User user = userService.getUserById(userId);
        note.setUser(user);
        noteRepository.save(note);
    }
}
