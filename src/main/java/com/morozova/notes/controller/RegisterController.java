package com.morozova.notes.controller;

import com.morozova.notes.model.User;
import com.morozova.notes.service.UserService;
import com.morozova.notes.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Flizaita on 17.11.2016.
 */
@RestController
@RequestMapping(value = "/register")
@ComponentScan(basePackages = "com.morozova.notes.service")
public class RegisterController {
    @Autowired
    private UserService userService;

    @PostMapping
    void register(@RequestBody User user, HttpServletResponse response) throws IOException{
        userService.saveUser(user);
        SecurityUtils.sendResponse(response, HttpServletResponse.SC_OK, "You were registered");
    }
}
