package com.morozova.notes.controller;

import com.morozova.notes.model.Chapter;
import com.morozova.notes.model.Note;
import com.morozova.notes.repository.ChapterRepository;
import com.morozova.notes.repository.NoteRepository;
import com.morozova.notes.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Flizaita on 26.11.2016.
 */

@RestController
public class ChapterController {

    @Autowired
    NoteRepository noteRepository;

    @Autowired
    UserService userService;

    @Autowired
    ChapterRepository chapterRepository;

    @RequestMapping(value = "/note/{noteId}/chapter/", method = RequestMethod.POST)
    void createChapter(@PathVariable Long noteId, @RequestBody Chapter chapter){
        Note note = noteRepository.findById(noteId);
        chapter.setNote(note);
        chapterRepository.save(chapter);
    }

    @RequestMapping(value = "/chapter/{chapterId}", method = RequestMethod.GET)
    Chapter getChapter(@PathVariable Long chapterId){
        return chapterRepository.findOne(chapterId);
    }

    @RequestMapping(value = "/chapter/{chapterId}", method = RequestMethod.PUT)
    Chapter editChapter(@PathVariable Long chapterId, @RequestBody Chapter chapter){
        return chapterRepository.save(chapter);
    }

    @RequestMapping(value = "/chapter/{chapterId}", method = RequestMethod.DELETE)
    void deleteChapter(@PathVariable Long chapterId){
        chapterRepository.delete(chapterId);
    }
}
