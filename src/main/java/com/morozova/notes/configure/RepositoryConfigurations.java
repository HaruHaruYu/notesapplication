package com.morozova.notes.configure;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Created by Flizaita on 17.11.2016.
 */
@Configuration
@EnableAutoConfiguration
@EnableJpaRepositories(basePackages = {"com.morozova.notes.repository"})
@EnableTransactionManagement
public class RepositoryConfigurations {
}
