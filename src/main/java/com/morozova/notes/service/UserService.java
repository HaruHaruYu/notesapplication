package com.morozova.notes.service;

import com.morozova.notes.model.User;

/**
 * Created by Flizaita on 17.11.2016.
 */
public interface UserService {


    User getUserById(Long id);

    Iterable<User> findAllUsers();

    boolean saveUser(User user);

    void updateUser(Long id);

    void deleteUser(Long id);

    User findByEmail(String email);

    boolean isValid(String password, String email);

}
