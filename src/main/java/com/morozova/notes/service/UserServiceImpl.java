package com.morozova.notes.service;

import com.morozova.notes.model.User;
import com.morozova.notes.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Flizaita on 17.11.2016.
 */
@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    public void setUserRepository(UserRepository userRepository){
        this.userRepository = userRepository;
    }

    @Override
    public User getUserById(Long id) {
        return userRepository.findOne(id);
    }

    @Override
    public Iterable<User> findAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public boolean saveUser(User user) {
        User newUser = userRepository.findByEmail(user.getEmail());
        if(newUser == null) {
            userRepository.save(user);
            return true;
        }
        return false;
    }

    @Override
    public void updateUser(Long id) {
        User user = userRepository.findOne(id);
        userRepository.save(user);
    }

    @Override
    public void deleteUser(Long id) {
        userRepository.delete(id);
    }

    @Override
    public User findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public boolean isValid(String password, String email){
        User user = userRepository.findByEmail(email);
        return user != null && (user.getPassword()).equals(password);
    }
}