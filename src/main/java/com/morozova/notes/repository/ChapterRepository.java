package com.morozova.notes.repository;

import com.morozova.notes.model.Chapter;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Flizaita on 24.11.2016.
 */
public interface ChapterRepository extends CrudRepository<Chapter, Long>{
}
