package com.morozova.notes.repository;

import com.morozova.notes.model.Note;
import com.morozova.notes.model.User;
import org.springframework.data.repository.CrudRepository;

import java.util.Collection;

/**
 * Created by Flizaita on 21.11.2016.
 */
public interface NoteRepository extends CrudRepository<Note, Long> {
    Collection<Note> findByUserId(Long UserId);
    Note findById(Long id);
}
