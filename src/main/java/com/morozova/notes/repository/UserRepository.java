package com.morozova.notes.repository;

import com.morozova.notes.model.User;
import org.springframework.data.repository.CrudRepository;


/**
 * Created by Flizaita on 17.11.2016.
 */
public interface UserRepository extends CrudRepository<User, Long> {
    User findByEmail(String email);
}
