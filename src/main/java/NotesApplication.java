import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;

/**
 * Created by Flizaita on 17.11.2016.
 */
@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan(basePackages = "com.morozova.notes.*")
@EntityScan(basePackages = "com.morozova.notes.model")
public class NotesApplication {
    public static void main(String[] args) {
        SpringApplication.run(NotesApplication.class, args);
    }

}
